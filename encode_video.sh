#!/bin/bash
for PPM in $@
do
  M2V="${PPM%ppm}m2v"
  ppmtoy4m -S 444 -F 25:1 <"${PPM}" |
  y4mscaler -I sar=1/1 -O preset=dvd -O yscale=1/1 |
  mpeg2enc -f 8 -q 3 -b 8000 -B 768 -D 10 -g 15 -G 15 -P -R 2 -o "${M2V}"
done
