#!/bin/bash
SEGMENT=$(ls -1 *.ppm | sort -R | head -n 1)
while [ "z${SEGMENT}" != "z" ]
do
  cat ${SEGMENT}
  SOURCE=$(echo ${SEGMENT} | sed 's|^.*_-_||' | sed 's|\.ppm$||')
  SEGMENT=$(ls -1 ${SOURCE}_-_*.ppm | sort -R | head -n 1)
done |
ppmtoy4m -S 420mpeg2 -F 25:1 |
mplayer -demuxer y4m -
